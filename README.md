![N|Solid](/client/src/assets/trust42_logo_black.png)

**Trust42** is a single page application done as the final project for Telerik Academy Alpha JS track.
It is an investmant platform that aims the management of high volume clients. Managers have access to the assigned clients’ portfolios, open positions (buy or sell), see live price data, view and search available instruments, access clients' watchlists of stocks, see history of closed positions.

The structure of the application is:

**Visible to  managers:**
1. Login page
2. General overview mode
    - Dashboard view with all stock available
    - List with all clients asigned to the respected manager
3. CLient management mode - on selected client
    - Client portfolio
    - Dashboard with all stocks where manager can buy and sell stocks for the client
    - List with active positions and option for closure
    - Watchlist for the selected client


**Visible only to all Administrators:**
1. Login page
2. Admin panel: 
    - create users, create managers, create clients
    - assign/re-asign clients to managers

 # Prerequisites
* [Node.js](https://nodejs.org/) 
* [Angular CLI](https://cli.angular.io)
* [MariaDB](https://downloads.mariadb.org/)
* [MySQL Workbench](https://www.mysql.com/products/workbench/) 



## Getting Started <a name="gettingStarted"></a>

To get a copy of the project running on your local machine you need to clone the repository into a brand new folder on your machine and navigate to the `server` folder and run:
```
npm install
```
- create in MySQL Workcbench **database**. In `ormconfig.json` and `ormconfig.js` default name is *testdb*, you can rename it but it needs to match the name in MySQL. See also password and port!
 - next step is `generate migration`, to create tables and their relations in database.
 ```sh
$ npm run migration:run -- --name=Initial
```
  - if the migration is successfull apply it to database.
  - if the migration fail, delete all generated migratons in this file and run it again)
 ```sh
$ npm run migration:run
```
 - create `.env` file at root level which include sensitive information for your server:
  ```sh
DB_DATABASE_NAME='your Database Name'
JWT_SECRET='password For Generate JWTokens'
```
 - to run the server use:
  ```sh
$ npm run start
```
 - or in development mode:
  ```sh
$ npm run start:dev
```
Now navigate to `client` folder, after that run `npm install`, to install all packages from `package.json` file.
```sh
$ npm install
```
 - to run application in development mode use:
 ```sh
$ ng serve
```
## Usage <a name ="Usage"></a>

To be able to experience the app's full capabilities, you will need to login with eigher as a Manager or as an Admin.
Navigate to the login page and use the following credentials:

Manager:
```
Username: manager@test.com
Password: TestPassword1!
```

Admin:
```
Username: admin@test.com
Password: TestPassword1!
```

# Built With
Trust42 uses the following open source frameworks and libraries:
 - [Node.js](https://nodejs.org/en/)
 - [NestJS](https://nestjs.com/) 
 - [Angular](https://angular.io/)
 - [Angular Material](https://material.angular.io/)
 - [CanvasJS](https://canvasjs.com/)
 - [MariaDB](https://mariadb.org/)
 - [TypeORM](http://typeorm.io/#/)
 - [JWT](https://jwt.io/)

 Angular and Angular Materuial was used to build the client-side logic and UI. Stock graphs were visually implemented with CanvasJS.
 NestJS provided functionality for communication with the model which was structured with MariaDB and TypeORM. 

# Authors
 - [Milen Rabadzhiev](https://gitlab.com/milenrab97)
 - [Nikolay Penev](https://gitlab.com/NikoPenev)

# Acknowledgments
Telerik Academy tech trainers:
 - [Martin Veshev](https://github.com/vesheff)
 - [Rosen Urkov](https://github.com/RosenUrkov)
 - [Steven Tsvetkov](https://github.com/StevenTsvetkov)


