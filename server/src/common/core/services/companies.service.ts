import { Industry } from '../../../data/entities/industry.entity';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from 'src/data/entities/company.entity';
import { Repository } from 'typeorm';
import { CompanyDTO } from 'src/models/company.dto';
import { Watchlist } from 'src/data/entities/watchlist.entity';
import { Price } from 'src/data/entities/prices.entity';

@Injectable()
export class CompaniesService {
    constructor(
        @InjectRepository(Company)
        private readonly companyRepository: Repository<Company>,
        @InjectRepository(Industry)
        private readonly industryRepository: Repository<Industry>,
        @InjectRepository(Watchlist)
        private readonly watchlistRepository: Repository<Watchlist>,
        @InjectRepository(Price)
        private readonly priceRepository: Repository<Price>,
    ) { }

    async createCompany(companyDTO: CompanyDTO) {
        const companyFound = await this.companyRepository.findOne({ where: { name: companyDTO.name } });

        if (companyFound) {
            throw new HttpException('Company already exists!', HttpStatus.BAD_REQUEST);
        }

        const industry = await this.industryRepository.findOne({ where: { id: companyDTO.industryId } });
        if (!industry) {
            throw new HttpException('Industry not found!', HttpStatus.NOT_FOUND);
        }

        const company = new Company();
        company.name = companyDTO.name;
        company.abbr = companyDTO.abbr;
        company.icon = companyDTO.icon;
        company.ceo = companyDTO.ceo;
        company.address = companyDTO.address;
        company.industry = industry;
        company.closedate = new Date();

        await this.companyRepository.create(company);

        return await this.companyRepository.save(company);
    }

    async updateCompany(id: string, companyDTO: Partial<CompanyDTO>) {
        const companyFound = await this.companyRepository.findOne({ where: { id } });

        if (!companyFound) {
            throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
        }

        companyFound.name = companyDTO.name;
        companyFound.abbr = companyDTO.abbr;
        companyFound.icon = companyDTO.icon;
        companyFound.ceo = companyDTO.ceo;
        companyFound.address = companyDTO.address;

        if (companyDTO.industryId) {
            const industry = await this.industryRepository.findOne({ where: { id: companyDTO.industryId } });
            if (!industry) {
                throw new HttpException('Industry not found!', HttpStatus.NOT_FOUND);
            }
            companyFound.industry = industry;
        }

        await this.companyRepository.save(companyFound);

        return await this.companyRepository.findOne({
            where: { id },
        });
    }

    async getCompaniesByIndustry(id: string) {
        const industry = await this.industryRepository.findOne({ where: { id } });

        if (!industry) {
            throw new HttpException('Industry not found!', HttpStatus.NOT_FOUND);
        }

        return await this.companyRepository.find({ where: { industry } });
    }

    async getCompanyTimesListed(id: string) {
        const companyFound = await this.companyRepository.findOne({ where: { id } });

        if (!companyFound) {
            throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
        }

        const companies = await this.watchlistRepository.find({ where: { companyFound } });

        return companies.length;
    }

    async getAll() {
        const companies = await this.companyRepository.find();
        return Promise.all(
            companies.map((c) => this._getSingleCompanyUtil(c)),
        );
        // return await this.companyRepository.find();
    }

    async _getSingleCompanyUtil(c: Company) {
        const price = await this.priceRepository.findOne({ where: { company: c }, order: { opendate: 'DESC' } });
        const buySellPrice = {
            buy: price.endprice,
            sell: 0.9923 * price.endprice,
        };
        return {
            ...c,
            lastPrice: buySellPrice,
        };
    }
}