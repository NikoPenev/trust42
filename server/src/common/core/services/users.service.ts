import { Watchlist } from './../../../data/entities/watchlist.entity';
import { ClientRegisterDTO } from './../../../models/user/client-register.dto';
import { GetUserDTO } from '../../../models/user/get-user.dto';
import { UserLoginDTO } from '../../../models/user/user-login.dto';
import { Injectable, BadRequestException } from '@nestjs/common';
import { Repository, AdvancedConsoleLogger } from 'typeorm';
import { User } from '../../../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

import * as bcrypt from 'bcrypt';
import { JwtPayload } from '../../../interfaces/jwt-payload';
import { Role } from '../../../data/entities/role.entity';
import { Funds } from '../../../data/entities/funds.entity';
import { RegisterDTO } from '../../../models/user/register.dto';
import { Client } from 'src/data/entities/client.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
    @InjectRepository(Funds)
    private readonly fundsRepository: Repository<Funds>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Watchlist)
    private readonly watchlistRepository: Repository<Watchlist>,
  ) { }

  // ==> Only admin can register new client and managers profiles
  async createManager(manager: RegisterDTO): Promise<User> {
    const foundManager = await this.usersRepository.findOne({ email: manager.email });
    if (foundManager) {
      throw new BadRequestException('Email already exist');
    }

    try {
      const managerRole = await this.roleRepository.findOne({ rolename: 'manager' });
      const newManager = await this.usersRepository.create();

      newManager.fullname = `${manager.firstName} ${manager.lastName}`;
      newManager.email = manager.email;
      newManager.password = manager.password = await bcrypt.hash(manager.password, 10);
      newManager.dateregistered = new Date();
      newManager.role = managerRole;

      return await this.usersRepository.save(newManager);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async createAdmin(admin: RegisterDTO): Promise<User> {
    const foundAdmin = await this.usersRepository.findOne({ email: admin.email });
    if (foundAdmin) {
      throw new BadRequestException('Email already exist');
    }
    try {
      const role = await this.roleRepository.findOne({ rolename: 'admin' });
      const newAdmin = await this.usersRepository.create();
      newAdmin.fullname = `${admin.firstName} ${admin.lastName}`;
      newAdmin.email = admin.email;
      newAdmin.dateregistered = new Date();
      newAdmin.role = role;
      newAdmin.password = admin.password = await bcrypt.hash(admin.password, 10);
      return await this.usersRepository.save(newAdmin);

    } catch (error) {
      throw new BadRequestException();
    }
  }

  async createClient(client: ClientRegisterDTO): Promise<Client> {
    const foundClient = await this.clientRepository.findOne({ email: client.email });
    if (foundClient) {
      throw new BadRequestException('Email already exist');
    }

    /*     try { */
    const manager = await this.usersRepository.findOne({ id: client.manager });

    const funds = await this.fundsRepository.create();
    funds.currentamount = +client.amount;
    await this.fundsRepository.save(funds);

    const watchlist = await this.watchlistRepository.create();
    await this.watchlistRepository.save(watchlist);

    const newClient = await this.clientRepository.create();
    newClient.firstName = client.firstName;
    newClient.lastName = client.lastName;
    newClient.address = client.address;
    newClient.dateregistered = new Date();
    newClient.age = client.age;
    newClient.email = client.email;
    newClient.manager = manager;
    newClient.funds = funds;
    newClient.watchlist = watchlist;

    return await this.clientRepository.save(newClient);
    /*     } catch (error) {
          throw new BadRequestException('Client cannot be created');
        } */
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    const userFound: any = await this.usersRepository.findOne({ where: { email: payload.email } });
    return userFound;
  }

  async signIn(user: UserLoginDTO): Promise<User> {
    const userFound: User = await this.usersRepository.findOne({ where: { email: user.email } });
    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }
    return null;
  }

  async getAllUsers() {
    return this.usersRepository.find({});
  }

  async getAllClients() {
    return this.clientRepository.find({});
  }

  async   getClient(id: string): Promise<Client> {
    try {
      const client = await this.clientRepository.findOne({ id });
      return client;
    } catch (error) {
      throw new BadRequestException('No such client');
    }
  }

  async getManager(id: string): Promise<User> {
    try {
      const manager = await this.usersRepository.findOne({ id });
      return manager;
    } catch (error) {
      throw new BadRequestException('No such manager');
    }
  }

  async assignClientManager(clientId: string, managerId: string) {
    const manager = await this.usersRepository.findOne({ where: { id: managerId } });
    const client = await this.clientRepository.findOne({ where: { id: clientId } });

    client.manager = manager;

    return await this.clientRepository.save(client);
  }

  async changePassword(email: string, password: string) {
    const user = await this.usersRepository.findOne({ where: { email } });

    const pw = await bcrypt.hash(password, 10);
    user.password = pw;
    await this.usersRepository.save(user);

    return true;
  }
}
