import { Injectable, HttpException, HttpStatus, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Between, MoreThan, LessThan } from 'typeorm';
import { Price } from 'src/data/entities/prices.entity';
import { Company } from 'src/data/entities/company.entity';
import { format } from 'date-fns';

// TypeORM Query Operators
export const MoreThanDate = (date: Date) => MoreThan(format(date, 'YYYY-MM-DD HH:MM:SS'));
export const LessThanDate = (date: Date) => LessThan(format(date, 'YYYY-MM-DD HH:MM:SS'));

@Injectable()
export class PricesService {

  constructor(
    @InjectRepository(Price)
    private readonly priceRepository: Repository<Price>,
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
) { }

  async getCompanyPrices(id: string, lastN: number, startdate?: Date, enddate?: Date): Promise<Price[]> {
    const company = await this.companyRepository.findOne({ id });
    if (!company) {
      throw new HttpException('Company doesn\'t exist!', HttpStatus.NOT_FOUND);
    }

    if (lastN) {
      return await this.priceRepository.find({ where: { company }, order: { opendate: 'DESC' }, take: lastN });
    }
    if (startdate && enddate) {
      return await this.priceRepository.find({ opendate: Between (startdate, enddate), company: Promise.resolve(company)});
    }
    if (startdate && !enddate) {
      return await this.priceRepository.find({ opendate: MoreThan (startdate.valueOf() - 1), company: Promise.resolve(company)});
    }

    return [await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } })];
  }

  async getLastPricePerCompany(): Promise<Price[]> {
    const companies = await this.companyRepository.find({});
    const result = [];

    for (const company of companies) {
      try {
        const price = await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } });
        result.push(price);
      } catch (e) {
        // Log error if necessary
      }
    }

    return result;
  }

  async getLastPriceForCompany(id: string): Promise<Price> {
    const company = await this.companyRepository.findOne({ where: { id } });

    const price = await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } });

    return price;
  }
}