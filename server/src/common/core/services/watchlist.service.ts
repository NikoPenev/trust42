import { Watchlist } from './../../../data/entities/watchlist.entity';
import { Company } from '../../../data/entities/company.entity';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from '../../../data/entities/client.entity';
import { Price } from '../../../data/entities/prices.entity';
@Injectable()
export class WatchlistService {

    private currentWatchlist: Watchlist;

    constructor(
        @InjectRepository(Client)
        private readonly clientRepository: Repository<Client>,

        @InjectRepository(Watchlist)
        private readonly watchlistRepository: Repository<Watchlist>,
        @InjectRepository(Price)

        private readonly priceRepository: Repository<Price>,
        @InjectRepository(Company)

        private readonly companyRepository: Repository<Company>,

    ) { }

    async getCompanies(clientID: string): Promise<Company[]> {

        const foundClient = await this.clientRepository.findOne({ where: { id: clientID } });
        if (!foundClient) {
            throw new HttpException('Client not found!', HttpStatus.NOT_FOUND);
        }
        const clientWatchlist = await foundClient.watchlist;
        const foundWatchlist = await this.watchlistRepository.findOne({ where: { id: clientWatchlist.id } });
        if (!foundWatchlist) {
            throw new HttpException('This client has no watchlist', HttpStatus.NOT_FOUND);
        }
        // saving watchlist companies until we update the watchlit route
        this.currentWatchlist = foundWatchlist;

        return Promise.all(
            foundWatchlist.companies.map((c) => this._getSingleCompanyUtil(c)),
        );
    }

    async _getSingleCompanyUtil(c: Company) {
        const price = await this.priceRepository.findOne({ where: { company: c }, order: { opendate: 'DESC' } });
        const buySellPrice = {
            buy: price.endprice,
            sell: 0.9923 * price.endprice,
        };
        return {
            ...c,
            lastPrice: buySellPrice,
        };
    }

    async addCompany(companyToAdd: Company): Promise<object> {
        try {
            const watchlistCompanies = await (this.currentWatchlist.companies);

            if (!watchlistCompanies) {
                throw new HttpException('Watchlist not defined', HttpStatus.NOT_FOUND);
            }

            for (const company of watchlistCompanies) {
                if (company.id === companyToAdd.id) {
                    throw new HttpException('Company already in watchlist', HttpStatus.NOT_ACCEPTABLE);
                }
            }
            // updating watchlist in service also in db
            watchlistCompanies.push(companyToAdd);
            await this.watchlistRepository.save(watchlistCompanies);

            return { result: `${companyToAdd.name} has been added to watchlist!` };

        } catch (error) {
            console.log(`error details: Error on method addCompany\n`);
            console.log(`error message: ${error}`);
            throw new HttpException('Cannot add company', HttpStatus.BAD_REQUEST);
        }

    }

    async removeCompany(companyId: string): Promise<object> {
        try {
            const watchlistCompanies = await this.currentWatchlist.companies;
            const initialNumberOfCompanies = watchlistCompanies.length;

            for (const [index, company] of watchlistCompanies.entries()) {
                if (company.id === companyId) {
                    watchlistCompanies.splice(index, 1);
                    break;
                }
            }

            if (initialNumberOfCompanies === watchlistCompanies.length) {
                throw new HttpException('Company not found in watchlist', HttpStatus.NOT_FOUND);
            }
            // updating watchlist in service also in db
            await this.watchlistRepository.save(this.currentWatchlist);

            return { result: `Company with id:${companyId} has been removed from watchlist!` };
        } catch (error) {
            console.log(`error details: Error on method removeCompany\n`);
            console.log(`error message: ${error}`);
            throw new HttpException('Cannot remove company', HttpStatus.BAD_REQUEST);
        }
    }

    async addToWatchlist(clientId: string, companyId: string) {
        const client = await this.clientRepository.findOne({ where: { id: clientId } });
        const company = await this.companyRepository.findOne({ where: { id: companyId } });

        const timesCompanyInWatchlist = client.watchlist.companies.filter((c) => c.id === company.id).length;

        if (timesCompanyInWatchlist === 0) {
            client.watchlist.companies.push(company);
        }

        return await this.watchlistRepository.save(client.watchlist);
    }

    async removeFromWatchlist(clientId: string, companyId: string) {
        const client = await this.clientRepository.findOne({ where: { id: clientId } });
        const company = await this.companyRepository.findOne({ where: { id: companyId } });

        const timesCompanyInWatchlist = client.watchlist.companies.filter((c) => c.id === company.id).length;

        if (timesCompanyInWatchlist === 1) {
            client.watchlist.companies = client.watchlist.companies.filter((c) => {
                return c.id !== company.id;
            });
        }

        return await this.watchlistRepository.save(client.watchlist);
    }
}