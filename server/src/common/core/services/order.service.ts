import { Funds } from './../../../data/entities/funds.entity';
import { Status } from './../../../data/entities/status.entity';
import { Company } from './../../../data/entities/company.entity';
import { Injectable, HttpException, HttpStatus, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/data/entities/user.entity';
import { Repository } from 'typeorm';
import { Order } from '../../../data/entities/order.entity';
import { OrderDTO } from '../../../models/order/order.dto';
import { Client } from 'src/data/entities/client.entity';
import { Price } from 'src/data/entities/prices.entity';

@Injectable()
export class OrderService {
    constructor(
        @InjectRepository(Order)
        private readonly orderRepository: Repository<Order>,
        @InjectRepository(User)
        private readonly userrRepository: Repository<User>,
        @InjectRepository(Company)
        private readonly companyRepository: Repository<Company>,
        @InjectRepository(Status)
        private readonly statusRepository: Repository<Status>,
        @InjectRepository(Client)
        private readonly clientRepository: Repository<Client>,
        @InjectRepository(Price)
        private readonly priceRepository: Repository<Price>,
        @InjectRepository(Funds)
        private readonly fundsRepository: Repository<Funds>,
    ) { }

    async createOrder(clientId: string, order: OrderDTO) {
        const foundUser: Client = await this.clientRepository.findOne({ where: { id: clientId } });
        if (!foundUser) {
            throw new HttpException('Client not found!', HttpStatus.NOT_FOUND);
        }

        const foundCompany: Company = await this.companyRepository.findOne({ where: { id: order.companyId } });
        if (!foundCompany) {
            throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
        }

        const foundStatus = await this.statusRepository.findOne({ where: { statusname: 'opened' } });
        if (!foundStatus) {
            throw new HttpException('Status not found!', HttpStatus.NOT_FOUND);
        }

        const amountNeeded = order.buyPrice * order.units;
        if (amountNeeded <= foundUser.funds.currentamount) {
            try {
                const createOrder: Order = await this.orderRepository.create();
                createOrder.opendate = order.openDate;
                createOrder.closedate = order.closeDate;
                createOrder.buyprice = order.buyPrice;
                createOrder.sellprice = order.sellPrice;
                createOrder.units = order.units;
                createOrder.client = Promise.resolve(foundUser);
                createOrder.status = foundStatus;
                createOrder.company = foundCompany;

                await this.orderRepository.save(createOrder);
            } catch (error) {
                throw new HttpException('Cannot create order', HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new HttpException('You are out of money', HttpStatus.BAD_REQUEST);
        }

    }

    async createBuyOrder(clientId: string, order: OrderDTO) {
        const foundUser: Client = await this.clientRepository.findOne({ where: { id: clientId } });
        if (!foundUser) {
            throw new HttpException('Client not found!', HttpStatus.NOT_FOUND);
        }

        const foundCompany: Company = await this.companyRepository.findOne({ where: { id: order.companyId } });
        if (!foundCompany) {
            throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
        }

        const foundStatus = await this.statusRepository.findOne({ where: { statusname: 'opened' } });
        if (!foundStatus) {
            throw new HttpException('Status not found!', HttpStatus.NOT_FOUND);
        }

        const amountNeeded = order.buyPrice * order.units;
        if (amountNeeded <= foundUser.funds.currentamount) {
            try {
                /*                 console.log('before', foundUser.funds.currentamount);
                                await this.clientRepository.save(foundUser);
                                console.log('after', foundUser.funds.currentamount); */
                const newAmm = foundUser.funds.currentamount - amountNeeded;
                await this.fundsRepository.update({ id: foundUser.funds.id }, { currentamount: newAmm });

                const createOrder: Order = await this.orderRepository.create();
                createOrder.opendate = order.openDate;
                createOrder.closedate = order.closeDate;
                createOrder.buyprice = order.buyPrice;
                createOrder.sellprice = order.sellPrice;
                createOrder.units = order.units;
                createOrder.action = 'buy';
                createOrder.client = Promise.resolve(foundUser);
                createOrder.status = foundStatus;
                createOrder.company = foundCompany;

                return await this.orderRepository.save(createOrder);
            } catch (error) {
                throw new HttpException('Cannot create order', HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new HttpException('You are out of money', HttpStatus.BAD_REQUEST);
        }
    }

    async createSellOrder(clientId: string, order: OrderDTO) {
        const foundUser: Client = await this.clientRepository.findOne({ where: { id: clientId } });
        if (!foundUser) {
            throw new HttpException('Client not found!', HttpStatus.NOT_FOUND);
        }

        const foundCompany: Company = await this.companyRepository.findOne({ where: { id: order.companyId } });
        if (!foundCompany) {
            throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
        }

        const foundStatus = await this.statusRepository.findOne({ where: { statusname: 'opened' } });
        if (!foundStatus) {
            throw new HttpException('Status not found!', HttpStatus.NOT_FOUND);
        }

        const amountNeeded = order.sellPrice * order.units;
        if (amountNeeded <= foundUser.funds.currentamount) {
            try {
                const newAmm = foundUser.funds.currentamount - amountNeeded;
                await this.fundsRepository.update({ id: foundUser.funds.id }, { currentamount: newAmm });

                const createOrder: Order = await this.orderRepository.create();
                createOrder.opendate = order.openDate;
                createOrder.closedate = order.closeDate;
                createOrder.buyprice = order.buyPrice;
                createOrder.sellprice = order.sellPrice;
                createOrder.units = order.units;
                createOrder.action = 'sell';
                createOrder.client = Promise.resolve(foundUser);
                createOrder.status = foundStatus;
                createOrder.company = foundCompany;

                return await this.orderRepository.save(createOrder);
            } catch (error) {
                throw new HttpException('Cannot create order', HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new HttpException('You are out of money', HttpStatus.BAD_REQUEST);
        }

    }

    async getOrdersAll() {
        try {
            const foundOrders = await this.orderRepository.find();
            return foundOrders;
        } catch (error) {
            throw new HttpException('Open orders not found!', HttpStatus.NOT_FOUND);
        }
    }

    async getOrdersByClient(id: string) {
        const foundOrder = await this.orderRepository.findOneOrFail({ where: { clientId: id } });

        if (!foundOrder) {
            throw new HttpException('Orders not found!', HttpStatus.NOT_FOUND);
        }

        return foundOrder;
    }

    async closeOrder(id: string): Promise<Order> {
        try {
            const order: Order = await this.orderRepository.findOne({ id });
            order.status = await this.statusRepository.findOne({ where: { statusname: 'closed' } });
            order.closedate = new Date();

            const resolvedClient = await Promise.resolve(order.client);
            const resolvedFund = await this.fundsRepository.findOne({ where: { client: resolvedClient } });

            let newAmm;
            const price = await this.priceRepository.findOne({ where: { company: order.company }, order: { opendate: 'DESC' } });
            if (order.action === 'buy') {
                order.sellprice = price.endprice * 0.9923;
                newAmm = +resolvedFund.currentamount + price.endprice * 0.9923 * order.units;
            } else {
                order.buyprice = price.endprice;
                newAmm = +resolvedFund.currentamount + order.sellprice * order.units +
                    (order.sellprice - order.buyprice) * order.units;
            }

            await this.fundsRepository.update({ id: resolvedFund.id }, { currentamount: newAmm });

            return await this.orderRepository.save(order);
        } catch (error) {
            throw new HttpException('Orders not found!', HttpStatus.NOT_FOUND);
        }

    }

    async getClosedOrders(id: string) {
        const status = await this.statusRepository.findOne({ where: { statusname: 'closed' } });
        const client = await this.clientRepository.findOne({ where: { id } });

        const foundClosedOrders = await this.orderRepository.find({
            where: {
                status,
                client,
            },
        });

        if (!foundClosedOrders) {
            throw new HttpException('Closed orders not found!', HttpStatus.NOT_FOUND);
        }

        return foundClosedOrders;
    }

    async getOpenOrders(id: string) {
        const status = await this.statusRepository.findOne({ where: { statusname: 'opened' } });
        const client = await this.clientRepository.findOne({ where: { id } });

        const foundOpenOrders = await this.orderRepository.find({
            where: {
                client,
                status,
            },
        });
        if (!foundOpenOrders) {
            throw new HttpException('Open orders not found!', HttpStatus.NOT_FOUND);
        }

        return Promise.all(
            foundOpenOrders.map((or) => this._addLastPrice(or)),
        );
    }

    async _addLastPrice(or: Order) {
        const price = await this.priceRepository.findOne({ where: { company: or.company }, order: { opendate: 'DESC' } });
        const buySellPrice = {
            buy: price.endprice,
            sell: 0.9923 * price.endprice,
        };
        return {
            ...or,
            lastPrice: buySellPrice,
        };
    }

    async getOrdersInInterval(start: Date, end: Date) {
        const foundOrdersInInrerval = await this.orderRepository.find({
            where: {
                opendate: start,
                closedate: end,
            },
        });

        if (!foundOrdersInInrerval) {
            throw new HttpException('Orders in set interval not found!', HttpStatus.NOT_FOUND);
        }

        return foundOrdersInInrerval;
    }
}