import { User } from '../../data/entities/user.entity';
import { Company } from '../../data/entities/company.entity';
import { Controller, Get, UseGuards, Param, Body, ValidationPipe, Query } from '@nestjs/common';
import { Roles, RolesGuard } from 'src/common';
import { AuthGuard } from '@nestjs/passport';
import { PricesService } from 'src/common/core/services/prices.service';
import { Price } from 'src/data/entities/prices.entity';
import { PriceRequestDTO } from 'src/models/prices/price-request.dto';
import { identity } from 'rxjs';
import { start } from 'repl';

@Controller('prices')
export class PricesController {

    constructor(private readonly pricesService: PricesService) { }

    // @Get()/* 
    // @Roles('manager')
    // @UseGuards(AuthGuard(), RolesGuard) */
    // async getLatestForAllCompanies(): Promise<Price[]> {
    //     return await this.pricesService.getLastPricePerCompany();
    // }

    @Get('company')/* 
    @Roles('manager')
    @UseGuards(AuthGuard(), RolesGuard) */
    async getPrices(@Body(new ValidationPipe({
        transform: true,
        whitelist: true,
    })) priceRequest: PriceRequestDTO): Promise<object> {

        return await this.pricesService.getCompanyPrices(priceRequest.id, priceRequest.lastN, priceRequest.startdate, priceRequest.enddate);
    }

    @Get(':id')
    async getManyPrices(
        @Param('id') id: string,
        @Query('amount') lastN: number,
        @Query('startdate') startdate: string,
        @Query('enddate') enddate: string,
    ) {
        return this.pricesService.getCompanyPrices(
            id,
            lastN,
            startdate ? new Date(startdate) : null,
            enddate ? new Date(enddate) : null,
        );
    }

    @Get(':id/last')/* 
    @Roles('manager')
    @UseGuards(AuthGuard(), RolesGuard) */
    async getPrice(@Param('id') id: string) {

        return await this.pricesService.getLastPriceForCompany(id);
    }
}
