import { Module } from '@nestjs/common';
import { CoreModule } from 'src/common/core/core.module';
import { AuthModule } from 'src/auth/auth.module';
import { PricesController } from './prices.controler';
import { OrdersController } from 'src/controllers/orders.controller';

@Module({
  imports: [CoreModule, AuthModule],
  controllers: [PricesController],
  providers: [PricesController],
})
export class PricesModule {}
