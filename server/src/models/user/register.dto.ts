import { IsString, Matches } from 'class-validator';

export class RegisterDTO {
    @IsString()
    firstName: string;

    @IsString()
    lastName: string;

    @IsString()
    @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;

    @IsString()
    email: string;
}