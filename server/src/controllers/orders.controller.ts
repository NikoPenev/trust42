import { CompaniesService } from './../common/core/services/companies.service';
import { AdminGuard } from './../common/guards/roles/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, UseGuards, Body, Post, Put, Param } from '@nestjs/common';
import { UsersService } from '../common/core/services/users.service';
import { RolesGuard, Roles } from 'src/common';
import { OrderService } from 'src/common/core/services/order.service';

@Controller('orders')
export class OrdersController {

    constructor(
        private readonly ordersService: OrderService,
    ) { }

    @Get(':id/open')
    getOpenOrdersOfclient(
        @Param('id') id: string,
    ) {
        return this.ordersService.getOpenOrders(id);
    }

    @Post(':id/buy')
    buy(
        @Param('id') id: string,
        @Body() body: any,
    ) {
        return this.ordersService.createBuyOrder(id, body);
    }

    @Post(':id/sell')
    sell(
        @Param('id') id: string,
        @Body() body: any,
    ) {
        return this.ordersService.createSellOrder(id, body);
    }

    @Get(':id/closed')
    getClosedOrdersOfclient(
        @Param('id') id: string,
    ) {
        return this.ordersService.getClosedOrders(id);
    }

    @Put('close/:id')
    closeOrder(
        @Param('id') id: string,
    ) {
        return this.ordersService.closeOrder(id);
    }
}
