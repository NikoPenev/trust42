import { CompaniesService } from './../common/core/services/companies.service';
import { AdminGuard } from './../common/guards/roles/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, UseGuards, Body, Post, Put, Param } from '@nestjs/common';
import { UsersService } from '../common/core/services/users.service';
import { RolesGuard, Roles } from 'src/common';

@Controller('companies')
export class CompaniesController {

    constructor(
        private readonly companiesService: CompaniesService,
    ) { }

    @Post('')
    create(@Body() company: any) {
        return this.companiesService.createCompany(company);
    }

    @Put(':id')
    update(@Param('id') id: any, @Body() company: any) {
        return this.companiesService.updateCompany(id, company);
    }

    @Get('')
    getAll() {
        return this.companiesService.getAll();
    }
}
