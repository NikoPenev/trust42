import { AdminGuard } from './../common/guards/roles/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, UseGuards, Param, Post, Body } from '@nestjs/common';
import { UsersService } from '../common/core/services/users.service';
import { RolesGuard, Roles } from 'src/common';

@Controller('users')
export class UsersController {

  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Get()
  all() {
    return this.usersService.getAllUsers();
  }

  @Get('client/:id')
    getClientById(
        @Param('id') id: string,
    ) {
        return this.usersService.getClient(id);
    }

  // TODO: get clients by managerId
  @Get('clients')
  // @Roles('admin', 'manager')
  // @UseGuards(AuthGuard(), RolesGuard)
  getAllClients() {
    return this.usersService.getAllClients();
  }

  @Get('managers')
  async getManagers() {
    const users = await this.all();
    return users.filter(user => user.role.rolename === 'manager');
  }

  @Get('managers/:id')
  async getManager(@Param('id') id: string) {
    return this.usersService.getManager(id);
  }

  @Post('managers')
  createManager(
    @Body() manager,
  ) {
    return this.usersService.createManager(manager);
  }

  @Post('clients')
  createClient(
    @Body() client,
  ) {
    return this.usersService.createClient(client);
  }

  @Post('admins')
  createAdmin(
    @Body() admin,
  ) {
    return this.usersService.createAdmin(admin);
  }

  @Post('assign/:clientId/:managerId')
  assign(
    @Param('clientId') clientId: string,
    @Param('managerId') managerId: string,
  ) {
    return this.usersService.assignClientManager(clientId, managerId);
  }

  @Post('password')
  changePassword(
    @Body() body: any,
  ) {
    return this.usersService.changePassword(body.email, body.password);
  }
}
