import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ClientManagementService {
    constructor(private http: HttpClient) { }

    public getClientById(id: any): Observable<any> {
        console.log('service', id);
        return this.http.get<any>(`http://localhost:5500/users/client/${id}`);
    }
}