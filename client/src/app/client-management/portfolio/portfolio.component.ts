import { ModesService } from './../../core/modes.service';
import { ClientManagementService } from './../client-management.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-portfolio',
  providers: [ClientManagementService],
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit, OnDestroy {
  // clientSubscription: Subscription;
  clientId: string;
  currentClient: any = {};


  constructor(
    private clientManagementService: ClientManagementService,
    private modesService: ModesService,
    ) { }
  ngOnInit() {
    // this.getClientId();
    this.modesService.clientId.subscribe((id) => {
      this.clientId = id;
    });
    this.getSelectedClient();
  }

  // getClientId(): void {
  //   this.modesService.clientId.subscribe(
  //     (id: string) => {
  //       this.clientId = id;
  //       this.getSelectedClient();
  //     });
  // }

  getSelectedClient(): void {
    this.clientManagementService
    .getClientById(this.clientId).subscribe(
      (res) => {
        this.currentClient = res;
        console.log(res);
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }

  ngOnDestroy() {
    // this.clientSubscription.unsubscribe();
  }
}
