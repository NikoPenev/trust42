import { WatchlistComponent } from './../watchlist/watchlist.component';
import { PortfolioModeGuard } from './../../core/guards/portfolio-mode-guard.service';
import { PortfolioComponent } from './../portfolio/portfolio.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClientDashboardComponent } from '../client-dashboard/client-dashboard.component';
import { OpenTradesComponent } from '../open-trades/open-trades.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'portfolio',
                component: PortfolioComponent,
                canActivate: [PortfolioModeGuard]
            },
            {
                path: 'dashboard',
                component: ClientDashboardComponent,
                canActivate: [PortfolioModeGuard]
            },
            {
                path: 'open-trades',
                component: OpenTradesComponent,
                canActivate: [PortfolioModeGuard]
            },
            {
                path: 'watchlist',
                component: WatchlistComponent,
                canActivate: [PortfolioModeGuard]
            },
        ]),
    ],
    exports: [RouterModule],
})
export class ClientManagementRoutingModule { }
