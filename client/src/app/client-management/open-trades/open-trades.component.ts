import { Component, OnInit } from '@angular/core';
import { ModesService } from '../../core/modes.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ClientManagementService } from '../client-management.service';

@Component({
  selector: 'app-open-trades',
  providers: [ClientManagementService],
  templateUrl: './open-trades.component.html',
  styleUrls: ['./open-trades.component.css']
})
export class OpenTradesComponent implements OnInit {

  clientId: string;
  currentClient: any = {};


  constructor(
    private clientManagementService: ClientManagementService,
    private modesService: ModesService,
    ) { }
  ngOnInit() {
    this.modesService.clientId.subscribe((id) => {
      this.clientId = id;
    });
    this.getSelectedClient();
  }

  getSelectedClient(): void {
    this.clientManagementService
    .getClientById(this.clientId).subscribe(
      (res) => {
        this.currentClient = res;
        console.log(res);
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }
}

