import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientManagementRoutingModule } from './routing/client-management-routing.module';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { CustomMaterialModule } from '../core/material.module';
import { FormsModule } from '@angular/forms';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { OpenTradesComponent } from './open-trades/open-trades.component';
import { WatchlistComponent } from './watchlist/watchlist.component';


@NgModule({
  declarations: [PortfolioComponent, ClientDashboardComponent, OpenTradesComponent, WatchlistComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    CustomMaterialModule,
    ClientManagementRoutingModule,
    SharedModule,
  ]
})
export class ClientManagementModule { }
