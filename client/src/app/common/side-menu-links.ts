import { ILink } from '../models/links/link-interface';

export const generalOverviewLinks: ILink[] = [
    {
        name: 'Dashboard',
        route: ['/', 'manager', 'dashboard'],
        icon: 'fas fa-chart-line fa-fw',
    },
    {
        name: 'Clients',
        route: ['/', 'manager', 'client-list'],
        icon: 'fas fa-users fa-fw',
    },
];

export const clientManagementLinks: ILink[] = [
    {
        name: 'Client Portfolio',
        route: ['/', 'client', 'portfolio'],
        icon: 'fas fa-id-card fa-fw',
    },
    {
        name: 'Dashboard',
        route: ['/', 'client', 'dashboard'],
        icon: 'fas fa-tachometer-alt fa-fw',
    },
    {
        name: 'Open Positions',
        route: ['/', 'client', 'open-trades'],
        icon: 'fas fa-hand-holding-usd fa-fw',
    },
    {
        name: 'Watchlist',
        route: ['/', 'client', 'watchlist'],
        icon: 'far fa-eye fa-fw',
    },
];

export const adminLinks: ILink[] = [
    {
        name: 'Assign Clients',
        route: ['/', 'admin', 'panel'],
        icon: 'fas fa-user-edit fa-fw',
    },
    {
        name: 'Create Users',
        route: ['/', 'admin', 'create'],
        icon: 'fas fa-user-plus fa-fw',
    },
    {
        name: 'Change Password',
        route: ['/', 'admin', 'change-password'],
        icon: 'fas fa-user-edit fa-fw',
    },

];
