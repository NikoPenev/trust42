import { HttpClient } from '@angular/common/http';
import { NotificationService } from './../../../core/notification.service';
import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-close-dialog',
  templateUrl: './close-dialog.component.html',
  styleUrls: ['./close-dialog.component.scss']
})
export class CloseDialogComponent implements OnInit {
  onPositionClose = new EventEmitter<boolean>();
  dialogData = {};
  order: any = {};
  invested = 0;
  profit = 0;
  units = 0;
  total;

  constructor(
    private notificator: NotificationService,
    private http: HttpClient,
    public dialogRef: MatDialogRef<CloseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.dialogData = this.data;
    this.order = this.data.order;

    if (this.order.action === 'buy') {
      this.handleBuyCase();
    } else {
      this.handleSellCase();
    }

    this.total = this.invested + this.profit;
  }

  handleBuyCase() {
    this.invested = this.order.buyprice * this.order.units;
    this.profit = this.order.lastPrice.sell * this.order.units - this.invested;
  }

  handleSellCase() {
    this.invested = this.order.sellprice * this.order.units;
    this.profit = this.invested - this.order.lastPrice.buy * this.order.units;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  confirmClose() {
    console.log('order', this.order);
    this.http.put(`http://localhost:5500/orders/close/${this.order.id}`, {})
    .subscribe((x) => {
      this.onPositionClose.emit(true);
      this.dialogRef.close(x);
      this.notificator.success('Position successfully closed!');
    });
  }
}
