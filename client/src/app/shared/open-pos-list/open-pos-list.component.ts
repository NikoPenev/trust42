import { Subscription } from 'rxjs';
import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { OpenPosListService } from './open-pos-list.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { CloseDialogComponent } from './close-dialog/close-dialog.component';
import { NotificationService } from '../../core/notification.service';

@Component({
  selector: 'app-open-pos-list',
  templateUrl: './open-pos-list.component.html',
  styleUrls: ['./open-pos-list.component.scss']
})
export class OpenPosListComponent implements OnInit, AfterViewInit {
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Input() client: any;
  @Input() action: boolean;
  @Input() isOpen: boolean;
  @Input() isInset: boolean;

  orders: any[];
  displayedColumns: string[];
  openPosColumns = ['market', 'opendate', 'units', 'invested', 'buysellprice', 'profit', 'act'];
  closePosColumns = ['market', 'opendate', 'closedate', 'units', 'invested', 'total', 'finalProfit'];


  constructor(
    private openPosListService: OpenPosListService,
    public dialog: MatDialog,
    private notificator: NotificationService,
  ) { }

  ngOnInit() {
    if (this.isOpen) {
      this.displayedColumns = this.openPosColumns;
      this.getActivePositions();
      if (this.action) {
        this.displayedColumns.push('action');
      }
    } else {
      this.getClosedPositions();
      this.displayedColumns = this.closePosColumns;
    }
    console.log('passed client', this.client);
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }
  getActivePositions() {
    this.openPosListService
      .getActivePositions(this.client.id)
      .subscribe(
        (res) => {
          this.orders = res;
          this.dataSource.data = res;
          console.log('orders', this.dataSource.data);
        },
        (err: HttpErrorResponse) => {
          console.log('err', err);
        }
      );
  }

  getClosedPositions() {
    this.openPosListService
      .getClosedPositions(this.client.id)
      .subscribe(
        (res) => {
          this.orders = res;
          this.dataSource.data = res;
          console.log('orders', this.dataSource.data);
        },
        (err: HttpErrorResponse) => {
          console.log('err', err);
        }
      );
  }

  openDialog(order) {
    const dialogRef = this.dialog.open(CloseDialogComponent, {
      data: {
        order: order
      }
    });

    let reload = false;
    const sub: Subscription = dialogRef.componentInstance.onPositionClose.subscribe((isClosed) => {
      reload = isClosed;
    });

    dialogRef.afterClosed().subscribe(result => {
      sub.unsubscribe();

      if (reload) {
        this.getActivePositions();
      }
    });
  }
}
