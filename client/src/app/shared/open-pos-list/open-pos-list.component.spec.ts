import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenPosListComponent } from './open-pos-list.component';

describe('OpenPosListComponent', () => {
  let component: OpenPosListComponent;
  let fixture: ComponentFixture<OpenPosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenPosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenPosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
