import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class OpenPosListService {
    constructor(private http: HttpClient) { }
    public getActivePositions(clientId: string): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/orders/${clientId}/open`);
    }
    public getClosedPositions(clientId: string): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/orders/${clientId}/closed`);
    }
}
