import { ModesService } from '../../core/modes.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core/auth.service';
import { NotificationService } from '../../core/notification.service';
import { generalOverviewLinks, clientManagementLinks, adminLinks } from '../../common/side-menu-links';
import { Subscription } from 'rxjs';
import { ILink } from 'src/app/models/links/link-interface';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit, OnDestroy {
  IsInClientModeSubscription: Subscription;
  isInClientMode = false;
  fullnameOfUser = '';
  userRole = '';
  links: ILink[] = [];
  clientId = '';
  clientFullName = '';


  constructor(
    private router: Router,
    private authService: AuthService,
    private notificator: NotificationService,
    private modesService: ModesService,
  ) { }

  ngOnInit() {
    this.fullnameOfUser = this.authService.getFullNameOfLoggedUser();
    this.userRole = this.authService.getUserRole();

    this.IsInClientModeSubscription = this.modesService.isInClientMode$
      .subscribe((flag) => {
        this.isInClientMode = flag;
        this.setLinks();

        if (flag) {
          this.getCurrentlyManagedUser();
        }
      });
  }

  ngOnDestroy() {
    this.IsInClientModeSubscription.unsubscribe();
  }

  logout() {
    this.authService.logout();
    this.notificator.success('Successfully logged out!');
    this.modesService.complete(false);
    this.router.navigate(['\login']);
  }

  completePortfolio(): void {
    this.modesService.complete();
  }

  getCurrentlyManagedUser(): void {
    this.modesService.clientId.subscribe((id) => {
      this.clientId = id;
    });
    this.modesService.clientFullName.subscribe((fullName) => {
      console.log('FULL NAME', fullName);
      this.clientFullName = fullName;
    });
  }

  private setLinks(): void {
    if (this.userRole === 'admin') {
      this.links = adminLinks;
    } else if (this.isInClientMode) {
      this.links = clientManagementLinks;
    } else {
      this.links = generalOverviewLinks;
    }
  }
}
