import { Component, OnInit, Input } from '@angular/core';
import * as CanvasJS from './../../canvasjs.min';
import { PricesService } from 'src/app/core/prices.service';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-spline-chart',
  templateUrl: './spline-chart.component.html',
  styleUrls: ['./spline-chart.component.scss']
})
export class SplineChartComponent implements OnInit {
  // companyId = '77e00760-cfab-40ec-890a-61e7a4fb3ec6';
  @Input() companyId: string;
  chart: any;
  dataPoints = [];
  constructor(
    private http: HttpClient,
    private pricesService: PricesService,
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.chart = new CanvasJS.Chart(this.companyId, {
        theme: 'dark1',
        exportEnabled: false,
        backgroundColor: '#424242',
        interactivityEnabled: false,
        zoomEnabled: true,
        animationEnabled: true,
        axisY: {
          title: '',
          tickLength: 0,
          lineThickness: 0,
          margin: 0,
          valueFormatString: ' ',
          gridThickness: 0,
/*           minimum: Math.min(...this.dataPoints.map(p => p.y)),
          maximum: Math.max(...this.dataPoints.map(p => p.y)), */
        },
        axisX: {
          title: '',
          tickLength: 0,
          margin: 0,
          lineThickness: 0,
          valueFormatString: ' ',
        },
        toolTip: {
          enabled: true,
        },
        data: [
          {
            type: 'line',
            dataPoints: this.dataPoints
          }]
      });

      this.getPrices();
    }, 250);
  }

  getPrices(): void {
    this.pricesService.getLastN(this.companyId, 10).subscribe(
      (res) => {
        this.convertToChartData(res);
        this.chart.render();
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }

  convertToChartData(rawPrices: any[]) {
    // open high low close
    rawPrices.forEach((price) => {
      this.dataPoints.push({
        x: new Date(price.opendate),
        y: price.endprice,
      });
    });
  }
}
