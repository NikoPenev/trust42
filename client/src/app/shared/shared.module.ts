import { WatchlistService } from './stock-list/watchlist.service';
import { CloseDialogComponent } from './open-pos-list/close-dialog/close-dialog.component';
import { BuySellDialogComponent } from './stock-list/buy-sell-dialog/buy-sell-dialog.component';
import { StockListService } from './stock-list/stock-list.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { CustomMaterialModule } from '../core/material.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StockListComponent } from './stock-list/stock-list.component';
import { CandlestickChartComponent } from './candlestick-chart/candlestick-chart.component';
import { SplineChartComponent } from './spline-chart/spline-chart.component';
import { OpenPosListComponent } from './open-pos-list/open-pos-list.component';
import { OpenPosListService } from './open-pos-list/open-pos-list.service';
import { StockDetailsComponent } from './stock-list/stock-details/stock-details.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { ProfitStyleDirective } from './directives/profit-style.directive';

@NgModule({
  declarations: [
    StockListComponent,
    CandlestickChartComponent,
    SplineChartComponent,
    BuySellDialogComponent,
    OpenPosListComponent,
    CloseDialogComponent,
    StockDetailsComponent,
    SideNavComponent,
    ProfitStyleDirective,
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    RouterModule,
  ],
  exports: [
    CustomMaterialModule,
    CommonModule,
    FormsModule,
    RouterModule,
    StockListComponent,
    CandlestickChartComponent,
    SplineChartComponent,
    BuySellDialogComponent,
    OpenPosListComponent,
    CloseDialogComponent,
    StockDetailsComponent,
    SideNavComponent,
    ProfitStyleDirective,
  ],
  providers: [
    StockListService,
    OpenPosListService,
    CurrencyPipe,
    WatchlistService,
  ],
  entryComponents: [
    BuySellDialogComponent,
    CloseDialogComponent,
    StockDetailsComponent,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: SharedModule };
  }
}
