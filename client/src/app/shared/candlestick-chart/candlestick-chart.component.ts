import { PricesService } from './../../core/prices.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Input, Inject } from '@angular/core';
import * as CanvasJS from './../../canvasjs.min';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-candlestick-chart',
  templateUrl: './candlestick-chart.component.html',
  styleUrls: ['./candlestick-chart.component.css']
})
export class CandlestickChartComponent implements OnInit {
  @Input() companyId: string;
  @Input() companyName: string;
  chart: any;
  numberOfPoints = 30;
  dataPoints = [];
  constructor(
    private http: HttpClient,
    private pricesService: PricesService,
  ) { }

  ngOnInit() {
    this.chart = new CanvasJS.Chart('chartContainer', {
      animationEnabled: true,
      theme: 'light2', // "light1", "light2", "dark1", "dark2"
      exportEnabled: true,
      title: {
        text: `${this.companyName} Stock Price`
      },
      subtitles: [{
        text: `for the last ${this.numberOfPoints} minutes`,
      }],
      axisX: {
        interval: 1,
        valueFormatString: 'hh:mm:ss'
      },
      axisY: {
        includeZero: false,
        prefix: '$',
        title: 'Price'
      },
      toolTip: {
        content: 'Date: {x}<br /><strong>Price:</strong><br />Open: {y[0]}, Close: {y[3]}<br />High: {y[1]}, Low: {y[2]}'
      },
      data: [{
        type: 'candlestick',
        yValueFormatString: '$##0.00',
        dataPoints: this.dataPoints
      }]
    });

    this.getPrices();
  }

  getPrices(): void {
    this.pricesService.getLastN(this.companyId, this.numberOfPoints).subscribe(
      (res) => {
        console.log(res);
        this.convertToChartData(res);
        this.chart.render();
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }

  convertToChartData(rawPrices: any[]) {
    // open high low close
    rawPrices.forEach((price) => {
      this.dataPoints.push({
        x: new Date(price.opendate),
        y: [
          price.startprice,
          price.highprice,
          price.lowprice,
          price.endprice,
        ],
      });
    });
  }
}
