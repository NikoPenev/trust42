import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ModesService } from 'src/app/core/modes.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../core/notification.service';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-buy-sell-dialog',
  templateUrl: './buy-sell-dialog.component.html',
  styleUrls: ['./buy-sell-dialog.component.scss']
})
export class BuySellDialogComponent implements OnInit {
  clientId: string;
  dialogData: any = {};
  tabIndex = 0;
  amount = 1000;

  constructor(
    private cp: CurrencyPipe,
    private notificator: NotificationService,

    private http: HttpClient,
    private modesService: ModesService,
    public dialogRef: MatDialogRef<BuySellDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.dialogData = this.data;
    this.tabIndex = this.data.tab;
    this.clientId = this.modesService.getClientId();

    console.log(this.data);
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  setOrder() {
    if (this.tabIndex === 0) {
      this.buy();
    } else {
      this.sell();
    }
  }

  buy() {
    const postBody: any = {
      openDate: new Date(),
      buyPrice: this.dialogData.company.lastPrice.buy,
      units: this.amount / this.dialogData.company.lastPrice.buy,
      companyId: this.dialogData.company.id,
    };

    this.http.post(`http://localhost:5500/orders/${this.clientId}/buy`, postBody)
    .subscribe((order: any) => {
      this.notificator.success(`Buy ${order.company.abbr} position opened for ${this.cp.transform(order.buyprice)}`);
    }, (err: any) => {
      this.notificator.error('Not enough money!');
    });
  }

  sell() {
    const postBody: any = {
      openDate: new Date(),
      sellPrice: this.dialogData.company.lastPrice.sell,
      units: this.amount / this.dialogData.company.lastPrice.sell,
      companyId: this.dialogData.company.id,
    };

    this.http.post(`http://localhost:5500/orders/${this.clientId}/sell`, postBody)
    .subscribe((order: any) => {
      this.notificator.success(`Sell ${order.company.abbr} position opened for ${this.cp.transform(order.sellprice)}`);
    }, (err: any) => {
      this.notificator.error('Not enough money!');
    });
  }

  onTabChange($event: any) {
    this.tabIndex = $event.index;
  }
}
