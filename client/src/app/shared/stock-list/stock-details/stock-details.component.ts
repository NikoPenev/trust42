import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-stock-details',
  templateUrl: './stock-details.component.html',
  styleUrls: ['./stock-details.component.css']
})
export class StockDetailsComponent implements OnInit {
  dialogData = {
    companyId: '77e00760-cfab-40ec-890a-61e7a4fb3ec6',
  };

  constructor(
    public dialogRef: MatDialogRef<StockDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    this.dialogData = this.data;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}
