import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class StockListService {
    constructor(private http: HttpClient) { }

    public getAllCompanies(company: any): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/companies`);
    }
}
