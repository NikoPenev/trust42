import { BuySellDialogComponent } from './buy-sell-dialog/buy-sell-dialog.component';
import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { StockListService } from './stock-list.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { StockDetailsComponent } from './stock-details/stock-details.component';
import { WatchlistService } from './watchlist.service';
import { ModesService } from '../../core/modes.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/core/notification.service';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.scss']
})
export class StockListComponent implements OnInit, AfterViewInit {
  dataSource = new MatTableDataSource<any>();
  companies: any[] = [];
  displayedColumns: string[] = ['name', 'change', 'buy', 'sell'];
  serviceInUse: any;
  clientId: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() isWatchlist: boolean;
  @Input() isInClientMode: boolean;

  constructor(
    private stockListService: StockListService,
    private watchlistService: WatchlistService,
    private modesService: ModesService,
    public dialog: MatDialog,
    public router: Router,
    private notificator: NotificationService,

  ) { }

  ngOnInit() {
    this.clientId = this.modesService.getClientId();
    if (this.isWatchlist) {
      this.displayedColumns.push('watchlist');
    }
    if (this.router.url === '/client/watchlist') {
      this.getWatchlistWithCompanies();
    } else {
      this.getListWithCompanies();
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getListWithCompanies(): void {
    this.stockListService
    .getAllCompanies({})
    .subscribe(
      (res) => {
        this.dataSource.data = res;
        console.log('companies', this.dataSource.data);
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }

  getWatchlistWithCompanies(): void {
    this.watchlistService
    .getAllCompanies(this.clientId)
    .subscribe(
      (res) => {
        this.dataSource.data = res;
        console.log('companies', this.dataSource.data);
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }

  openDialog(tab, company) {
    this.dialog.open(BuySellDialogComponent, {
      height: '400px',
      width: '600px',
      data: {
        tab: tab,
        company: company
      }
    });
  }

  openDatailsDialog(companyId: string, companyName: string) {
    console.log(companyId);
    this.dialog.open(StockDetailsComponent, {
      data: {
        companyId,
        companyName,
      }
    });
  }

  doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  addToWatchlist(company: any) {
    this.watchlistService.add(this.clientId, company.id).subscribe((res) => {
      this.notificator.success('Successfully added to watchlist.');
    });
  }

  removeFromWatchlist(company: any) {
    this.watchlistService.remove(this.clientId, company.id).subscribe((res) => {
      this.notificator.success('Successfully removed from watchlist.');
      this.getWatchlistWithCompanies();
    });
  }
}
