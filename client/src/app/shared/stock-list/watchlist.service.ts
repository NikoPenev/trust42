import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class WatchlistService {
    constructor(private http: HttpClient) { }

    public getAllCompanies(clientId: string): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/watchlist/${clientId}`);
    }

    public add(clientId: string, companyId: string): Observable<any> {
        return this.http.post<any>(`http://localhost:5500/watchlist/${clientId}/${companyId}`, {});
    }

    public remove(clientId: string, companyId: string): Observable<any> {
        return this.http.delete<any>(`http://localhost:5500/watchlist/${clientId}/${companyId}`, {});
    }
}
