import { Directive, ElementRef, Input, Renderer2, OnInit } from '@angular/core';

@Directive({
    selector: '[appProfitStyle]'
})
export class ProfitStyleDirective implements OnInit {
    constructor(private el: ElementRef, private renderer: Renderer2) { }
    @Input('appProfitStyle') profit: string;

    ngOnInit() {
        this.setStyles(this.profit);
    }
    private setStyles(profit: string) {
        const profitAsNum = Number(this.profit);
        let color = 'black';
        if (profitAsNum < 0) {
            color = 'red';
        } else if (profitAsNum > 0) {
            color = 'green';
        }

        this.renderer.setStyle(this.el.nativeElement, 'color', color);
        this.renderer.setStyle(this.el.nativeElement, 'font-weight', 'bold');
        this.renderer.setStyle(this.el.nativeElement, 'font-size', '120%');
    }
}
