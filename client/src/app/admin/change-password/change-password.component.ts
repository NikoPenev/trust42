import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AdminPanelService } from '../services/admin-panel.service';
import { NotificationService } from 'src/app/core/notification.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  passwordChangeForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private adminServ: AdminPanelService,
    private notificator: NotificationService,
  ) { }

  ngOnInit() {
    this.passwordChangeForm = this.fb.group({
      email: '',
      password: '',
    });
  }

  save() {
    this.adminServ.changePassword(this.passwordChangeForm.value).subscribe((res) => {
      this.notificator.success('Password successfully changed!');
    }, (err) => {
      this.notificator.error('Something went wrong!');
    });
  }
}
