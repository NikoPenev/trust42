import { RegisterClient } from './../../models/users/register-client';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { AdminPanelService } from '../services/admin-panel.service';
import { ClientListService } from 'src/app/general-overview/client-list/client-list.service';
import { AuthService } from '../../core/auth.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../core/notification.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  providers: [ClientListService],
  styleUrls: ['./admin-panel.component.scss']
})

export class AdminPanelComponent implements OnInit {
  clientForm: FormGroup;
  managerForm: FormGroup;
  adminForm: FormGroup;
  client: RegisterClient = new RegisterClient();
  typeOfRegistration = 'Client';

  clients: [];
  managers: [];
  fullnameOfUser = '';

  filteredClients: [];
  filteredManagers: [];

  _searchClients = '';
  _searchManagers = '';


  get searchClients(): string {
    return this._searchClients;
  }

  set searchClients(client: string) {
    this._searchClients = client;
    this.filteredClients = this.searchClients ? this.filterClientsByName(this.searchClients) : this.clients;
  }

  get searchManagers(): string {
    return this._searchManagers;
  }

  set searchManagers(manager: string) {
    this._searchManagers = manager;
    this.filteredManagers = this.searchManagers ? this.filterManagersByName(this.searchManagers) : this.managers;
  }

  constructor(
    private clientListService: ClientListService,
    private adminPanelService: AdminPanelService,
    private authService: AuthService,
    private router: Router,
    private notificator: NotificationService,
    private fb: FormBuilder,
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.getListWithClients();
    this.getListWithManagers();
    this.fullnameOfUser = this.authService.getFullNameOfLoggedUser();
  }

  logout() {
    this.authService.logout();
    this.notificator.success('Successfully logged out!');
    this.router.navigate(['/login']);
  }

  // Handle single selection for managers
  handleSelection(event) {
    if (event.option.selected) {
      event.source.deselectAll();
      event.option._setSelected(true);
    }
  }

  getListWithClients(): void {
    this.clientListService.getAllClients({
    }).subscribe(
      (res) => {
        this.clients = res;
        this.filteredClients = res;
        console.log(res);
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }
  filterClientsByName(name: string): any {
    name = name.toLocaleLowerCase();
    return this.clients.filter((client: any) => {
      return [client.firstName, client.lastName]
          .join(' ')
          .toLocaleLowerCase()
          .indexOf(name) !== -1;
    });
  }

  getListWithManagers(): void {
    this.adminPanelService.getAllManagers({
    }).subscribe(
      (res) => {
        this.managers = res;
        this.filteredManagers = res;
        console.log(res);
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }
  filterManagersByName(name: string): any {
    name = name.toLocaleLowerCase();
    return this.managers.filter((manager: any) => {
      return manager.fullname
          .toLocaleLowerCase()
          .indexOf(name) !== -1;
    });
  }

  link(manager, client) {
    console.log('Link', manager, 'to', client);
    this.adminPanelService
    .linkClientToManager(client, manager)
    .subscribe(
      (res) => {
        console.log(res);
        this.notificator.success('Client linked to Manager!');
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }
}
