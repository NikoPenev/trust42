import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './routing/admin-routing.module';
import { CustomMaterialModule } from '../core/material.module';
import { SharedModule } from '../shared/shared.module';
import { CreateUsersComponent } from './create-users/create-users.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AdminPanelService } from './services/admin-panel.service';

@NgModule({
  declarations: [
    AdminPanelComponent,
    CreateUsersComponent,
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CustomMaterialModule,
    AdminRoutingModule,
  ],
  providers: [
    AdminPanelService,
  ],
})
export class AdminModule { }
