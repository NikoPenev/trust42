import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AdminPanelService {
    constructor(private http: HttpClient) { }

    public getAllManagers(manager: any): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/users/managers`);
    }

    public linkClientToManager(clientId: string, managerId: string): Observable<any> {
        return this.http.post<any>(`http://localhost:5500/users/assign/${clientId}/${managerId}`, {});
    }

    public changePassword(body: any) {
        return this.http.post<any>(`http://localhost:5500/users/password`, body);
    }
}
