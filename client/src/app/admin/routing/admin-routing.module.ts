import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminPanelComponent } from '../admin-panel/admin-panel.component';
import { CreateUsersComponent } from '../create-users/create-users.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'panel', component: AdminPanelComponent },
            { path: 'create', component: CreateUsersComponent },
            { path: 'change-password', component: ChangePasswordComponent },
        ]),
    ],
    exports: [RouterModule],
})
export class AdminRoutingModule { }
