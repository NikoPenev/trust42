import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { ClientListService } from 'src/app/general-overview/client-list/client-list.service';
import { AuthService } from '../../core/auth.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../core/notification.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { AdminPanelService } from '../services/admin-panel.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-create-users',
  templateUrl: './create-users.component.html',
  providers: [ClientListService],
  styleUrls: ['./create-users.component.scss']
})
export class CreateUsersComponent implements OnInit {
  clientForm: FormGroup;
  managerForm: FormGroup;
  adminForm: FormGroup;
  typeOfRegistration = 'Client';
  myControl = new FormControl();
  filteredManagers: Observable<string[]>;
  managerOptions: any[];

  constructor(
    private clientListService: ClientListService,
    private adminPanelService: AdminPanelService,
    private authService: AuthService,
    private router: Router,
    private notificator: NotificationService,
    private fb: FormBuilder,
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.clientForm = this.fb.group({
      firstName: '',
      lastName: '',
      email: '',
      amount: '',
      age: '',
      address: '',
      manager: '',
    });

    this.managerForm = this.fb.group({
      firstName: '',
      lastName: '',
      email: '',
      password: '',
    });

    this.adminForm = this.fb.group({
      firstName: '',
      lastName: '',
      email: '',
      password: '',
    });

    this.getListWithManagers();

  }

  onTabChange($event: any) {
    this.typeOfRegistration = $event.tab.textLabel;
  }

  save() {
    if (this.typeOfRegistration === 'Client') {
      console.log('klieksadbkla;s');
      this.saveClient();
    } else if (this.typeOfRegistration === 'Manager') {
      console.log('merindjeisadas');
      this.saveManager();
    } else {
      console.log('admin');
      this.saveAdmin();
    }
  }

  saveClient() {
    return this.http.post('http://localhost:5500/users/clients', this.clientForm.value).subscribe(
      (response) => {
        this.notificator.success('Clinet successfully added!');
        console.log(response);
        this.clientForm.reset();
      }
    );
  }
//   displayFn(manager): string {
//     console.log(manager);
//       return manager ? manager.fullname : manager;
// }

  saveManager() {
    return this.http.post('http://localhost:5500/users/managers', this.managerForm.value).subscribe(
      (response) => {
        console.log(response);
        this.notificator.success('Manager successfully added!');
        this.clientForm.reset();

      }
    );
  }

  saveAdmin() {
    return this.http.post('http://localhost:5500/users/admins', this.adminForm.value).subscribe(
      (response) => {
        console.log(response);
          this.notificator.success('Admin successfully added!');
          this.clientForm.reset();

      }
    );
  }
  getListWithManagers(): void {
    this.adminPanelService.getAllManagers({
    }).subscribe(
      (res) => {
        this.managerOptions = res;
        console.log(res);
        this.filteredManagers = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }

   _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.managerOptions.filter(option => option.fullname.toLowerCase().includes(filterValue));
  }
}
