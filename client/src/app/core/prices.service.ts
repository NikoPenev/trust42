import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class PricesService {
    constructor(private http: HttpClient) { }

    public getLastN(companyId: string, lastN = 30): Observable<any> {
        const res = this.http.get<any>(`http://localhost:5500/prices/${companyId}?amount=${lastN}`);
        console.log('PRICESERV', res);
        return res;
    }
}
