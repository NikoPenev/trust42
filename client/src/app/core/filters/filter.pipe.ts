import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterClient'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
  transform(items: any[], field: string, value: string): any[] {
    console.log(items, field, value);
    if (!items) { return []; }
    if (!value) { return items; }
    return items.filter(it => it[field] === value);
  }
}
