import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthService {
    constructor(
        private http: HttpClient,
    ) { }

    public login(user: any): Observable<any> {
        return this.http.post<any>(`http://localhost:5500/auth/login`, user);
    }

    public logout(): void {
        localStorage.clear();
    }

    public isAuthenticated(): boolean {
        const token = this.getDecodedToken();

        if (!token) {
            return false;
        }

        return (token.exp - token.iat > 0);
    }
    public getFullNameOfLoggedUser() {
        const decodedToken = this.getDecodedToken();

        return !!decodedToken && decodedToken.fullname;
    }

    public getUserId(): string {
        return this.getDecodedToken().id;
    }

    public getUserRole() {
        const decodedToken = this.getDecodedToken();
        console.log(decodedToken);

        return !!decodedToken && decodedToken.role;
    }

    private getDecodedToken() {
        const token: string = localStorage.getItem('token');

        if (!token) {
            return null;
        }

        return jwt_decode(token);
    }
}
