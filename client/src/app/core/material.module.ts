import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatProgressSpinnerModule, MatSidenavModule,
  MatListModule, MatExpansionModule, MatChipsModule, MatGridListModule, MatTabsModule,
  MatPaginatorModule, MatSortModule, MatAutocompleteModule, MatProgressBarModule, MatTooltipModule
} from '@angular/material';
@NgModule({
  imports: [
  CommonModule,
  MatToolbarModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatTableModule,
  MatMenuModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatSidenavModule,
  MatListModule,
  MatExpansionModule,
  MatChipsModule,
  MatGridListModule,
  MatTabsModule,
  MatPaginatorModule,
  MatSortModule,
  MatAutocompleteModule,
  MatTooltipModule
  ],
  exports: [
  CommonModule,
   MatToolbarModule,
   MatButtonModule,
   MatCardModule,
   MatInputModule,
   MatDialogModule,
   MatTableModule,
   MatMenuModule,
   MatIconModule,
   MatProgressSpinnerModule,
   MatProgressBarModule,
   MatSidenavModule,
   MatListModule,
   MatExpansionModule,
   MatChipsModule,
   MatGridListModule,
   MatTabsModule,
   MatPaginatorModule,
   MatSortModule,
   MatAutocompleteModule,
   MatTooltipModule,
   ],
})
export class CustomMaterialModule { }
