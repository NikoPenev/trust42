

import { NgModule } from '@angular/core';
import { AuthService } from './auth.service';
import { AuthGuard } from './guards/auth.guard';
import { AlreadySignedGuard } from './guards/already-signed.guard';
import { NotificationService } from './notification.service';
import { PricesService } from './prices.service';
import { ModesService } from './modes.service';
import { PortfolioModeGuard } from './guards/portfolio-mode-guard.service';

@NgModule({
    imports: [],
    providers: [
        { provide: AuthService, useClass: AuthService },
        { provide: AuthGuard, useClass: AuthGuard},
        { provide: AlreadySignedGuard, useClass: AlreadySignedGuard},
        { provide: NotificationService, useClass: NotificationService},
        { provide: PricesService, useClass: PricesService},
        { provide: ModesService, useClass: ModesService},
        { provide: PortfolioModeGuard, useClass: PortfolioModeGuard},
    ],
})
export class CoreModule { }
