import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { AlreadySignedGuard } from './guards/already-signed.guard';
import { NotFoundComponent } from '../not-found/not-found.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [AlreadySignedGuard] },
  { path: '', redirectTo: '/manager/dashboard', pathMatch: 'full'},
  { path: 'manager', loadChildren: '../general-overview/general-overview.module#GeneralOverviewModule' },
  { path: 'client', loadChildren: '../client-management/client-management.module#ClientManagementModule' },
  { path: 'admin', loadChildren: '../admin/admin.module#AdminModule' },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '404' },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
