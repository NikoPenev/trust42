import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ModesService {
    private isInClientModeSubject = new BehaviorSubject<boolean>(this.clientSavedCheck());
    private clientIdSubject: BehaviorSubject<string> = new BehaviorSubject<string>(this.getClientId());
    private clientFullNameSubject: BehaviorSubject<string> = new BehaviorSubject<string>(this.getClientFullName());

    constructor(
        private router: Router,
    ) { }

    public get isInClientMode$(): Observable<boolean> {
        return this.isInClientModeSubject.asObservable();
    }

    public get clientId(): Observable<string> {
        return this.clientIdSubject.asObservable();
    }

    public get clientFullName(): Observable<string> {
        return this.clientFullNameSubject.asObservable();
    }

    manage(id: string, firstName: string, lastName: string): void {
        const fullName = firstName + ' ' + lastName;
        this.isInClientModeSubject.next(true);
        this.clientIdSubject = new BehaviorSubject<string>(id);
        this.clientFullNameSubject = new BehaviorSubject<string>(fullName);

        localStorage.setItem('clientId', id);
        localStorage.setItem('clientFullName', fullName);

        this.router.navigateByUrl('/client/portfolio');
    }

    complete(redirect = true): void {
        this.isInClientModeSubject.next(false);
        this.clientIdSubject.next('');

        localStorage.setItem('clientId', '');
        if (redirect) {
            this.router.navigateByUrl('/manager/dashboard');
        }
    }

    public getClientId(): string {
        return localStorage.getItem('clientId');
    }

    private getClientFullName(): string {
        return localStorage.getItem('clientFullName');
    }

    private clientSavedCheck(): boolean {
        const clientId = localStorage.getItem('clientId');
        return !!clientId && clientId !== '';
    }
}
