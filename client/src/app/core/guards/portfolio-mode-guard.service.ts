import { ModesService } from 'src/app/core/modes.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { NotificationService } from '../notification.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class PortfolioModeGuard implements CanActivate {
    constructor(
        private router: Router,
        private notificator: NotificationService,
        private modesService: ModesService,
    ) { }

    canActivate(): Observable<boolean> {
        return this.modesService.isInClientMode$.pipe(
            tap((inClientMode: boolean) => {
                if (!inClientMode) {
                    this.router.navigate(['/manager/client-list']);
                    this.notificator.error('You are not in client management mode!');
                }
            })
        );
    }
}
