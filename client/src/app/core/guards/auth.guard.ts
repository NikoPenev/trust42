import { AuthService } from './../auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NotificationService } from '../notification.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private authService: AuthService,
        private router: Router,
        private notificator: NotificationService,
    ) { }

    public canActivate(): boolean {
        const isAuthenticated = this.authService.isAuthenticated();

        if (isAuthenticated) {
            return true;
        }

        this.router.navigate(['/login']);
        this.notificator.error('Not authenticated');
        return false;
    }
}
