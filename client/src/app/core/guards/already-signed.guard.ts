import { AuthService } from './../auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NotificationService } from '../notification.service';

@Injectable()
export class AlreadySignedGuard implements CanActivate {
    constructor(
        private authService: AuthService,
        private router: Router,
        private notificator: NotificationService,
    ) { }

    public canActivate(): boolean {
        if (!this.authService.isAuthenticated()) {
            return true;
        }

        this.router.navigate(['']);
        this.notificator.error('Already signed in!');
        return false;
    }
}
