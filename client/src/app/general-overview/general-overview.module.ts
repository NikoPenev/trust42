import { CustomMaterialModule } from './../core/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralOverviewRoutingModule } from './routing/general-overview-routing.module';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientListComponent } from './client-list/client-list.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    DashboardComponent,
    ClientListComponent,
  ],
  imports: [
    CustomMaterialModule,
    AgGridModule.withComponents([]),
    FormsModule,
    CommonModule,
    GeneralOverviewRoutingModule,
    SharedModule,
  ]
})
export class GeneralOverviewModule { }
