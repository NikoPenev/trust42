import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ClientListService {
    constructor(private http: HttpClient) { }

    public getAllClients(client: any): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/users/clients`);
    }

    public getManager(managerId: string): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/users/managers/${managerId}`);
    }

    public getActivePositions(clientId: string): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/orders/${clientId}/open`);
    }

    public getClosedPositions(clientId: string): Observable<any> {
        return this.http.get<any>(`http://localhost:5500/orders/${clientId}/closed`);
    }
}
