import { AuthService } from './../../core/auth.service';
import { ClientListService } from './client-list.service';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ModesService } from 'src/app/core/modes.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  providers: [ClientListService],
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  managerId = '';
  clients: [];
  filteredClients: [];
  _searchText = '';

  get searchText(): string {
    return this._searchText;
  }

  set searchText(text: string) {
    this._searchText = text;
    this.filteredClients = this.searchText ? this.filterClientsByName(this.searchText) : this.clients;
  }

  constructor(
    private clientListService: ClientListService,
    private modesService: ModesService,
    private authService: AuthService,
    ) { }

  ngOnInit() {
    this.managerId = this.authService.getUserId();
    this.getListWithClients();
  }
  getListWithClients(): void {
    this.clientListService.getManager(this.managerId)
    .subscribe(
      (res) => {
        this.clients = res.clients;
        this.filteredClients = res.clients;
        console.log(res);
      },
      (err: HttpErrorResponse) => {
        console.log('err', err);
      });
  }

  filterClientsByName(name: string): any {
    name = name.toLocaleLowerCase();
    return this.clients.filter((client: any) => {
      return [client.firstName, client.lastName]
          .join(' ')
          .toLocaleLowerCase()
          .indexOf(name) !== -1;
    });
  }

  manage(clientId: string, clientName: string, clientSurname: string): void {
    this.modesService.manage(clientId, clientName, clientSurname);
  }
}
