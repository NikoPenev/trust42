import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';


@Component({
  selector: 'app-dashboard',
  providers: [DashboardService],
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  // AG-grid
//   columnDefs = [
//     {headerName: 'Markets', field: 'market', sortable: true, filter: true },
//     {headerName: 'Change', field: 'change' },
//     {headerName: 'Buy', field: 'buy'},
//     {headerName: 'Sell', field: 'sell'}
// ];


constructor() { }

  ngOnInit() {
  }

  // Ag-grid
  // prepareTableData(res) {
  //   console.log('res', res);
  //   const rowData: any[] = [];
  //   res.forEach(element => {
  //     const row = {
  //       market: element.name,
  //       change: 'Change',
  //       buy: 'Buy',
  //       sell: 'Sell',
  //     };
  //     rowData.push(row);
  //     console.log('row data', this.rowData);

  //   });
  //   return rowData;

  // }

}
