import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { ClientListComponent } from '../client-list/client-list.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
            { path: 'dashboard', component: DashboardComponent },
            { path: 'client-list', component: ClientListComponent },
        ]),
    ],
    exports: [RouterModule],
})
export class GeneralOverviewRoutingModule { }
