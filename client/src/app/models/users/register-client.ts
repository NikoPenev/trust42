export class RegisterClient {
    firstName: string;
    lastName: string;
    email: string;
    amount: number;
    age: number;
    address: number;
    managerId?: string;
}
