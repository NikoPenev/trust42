export class SignedUser {
    public email: string;
    public role: string;
    public fullname: string;
}
