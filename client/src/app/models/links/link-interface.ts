export class ILink {
   name: string;
   route: string[];
   icon: string;
}
