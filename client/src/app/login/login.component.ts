import { AuthService } from './../core/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from '../core/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(
    private router: Router,
    private authService: AuthService,
    private notificator: NotificationService,
  ) { }
  username: string;
  password: string;
  ngOnInit() {
  }
  login(): void {
    this.authService.login({
      email: this.username,
      password: this.password
    }).subscribe(
      (res) => {
        localStorage.setItem('token', res.token);
        this.notificator.success('Successfully logged in!');
        // this.router.navigate(['manager/dashboard']);

        const userRole = this.authService.getUserRole();
        switch (userRole) {
          case 'admin': {
              this.router.navigate(['admin/panel']);
             return true;
          }
          case 'manager': {
            this.router.navigate(['manager/dashboard']);
             return true;
          }
        }
      },
      (err: HttpErrorResponse) => {
        this.notificator.error('Invalid username and/or password!');
      });
  }
}

